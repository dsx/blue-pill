#include "stm32f1xx.h"
#include "clock.h"
#include "gpio.h"

// https://fr.wikipedia.org/wiki/Fr%C3%A9quences_des_touches_du_piano

#define FRQ 880

int
main (void) {
    uint8_t psc = 0; 
    clock_hse();             // 8 MHz
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_TIM1EN;
    gpio_pin_init(GPIOA, 8, O02_AF_PP);

    while (SystemCoreClock / ((psc + 1) * FRQ) > 65535) { psc++; }

    TIM1->PSC  = psc;
    TIM1->ARR  = SystemCoreClock / ((psc + 1) * FRQ);
    TIM1->CCR1 = TIM1->ARR / 2;

    TIM1->CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1;
    TIM1->CCER  |= TIM_CCER_CC1E;
    TIM1->BDTR  |= TIM_BDTR_MOE;
    TIM1->CR1   |= TIM_CR1_CEN;

    return 0;
}
