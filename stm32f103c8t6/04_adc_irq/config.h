#pragma once

#include "stm32f1xx.h"

#define ONBOARD_LED_GPIO  GPIOC
#define ONBOARD_LED_PIN   13

#define POTENTIOMETER_ADC      ADC1
#define POTENTIOMETER_CHANNEL  1
#define POTENTIOMETER_CR2      ADC_CR2_EXTSEL_2 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_0 // 111: External event select for regular group == SoftWareSTART

#define CFG_APB2EN  RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPCEN // GPIO A: potentiometer, GPIO C: onboard led
