#include "stm32f1xx.h"
#include "gpio.h"
#include "systick.h"

#include "config.h"

int
main (void) {
    RCC->CR |= RCC_CR_HSEON;                   // set High Speed External ON (== enable)
    while ( !(RCC->CR & RCC_CR_HSERDY) );      // wait for High Speed External ReaDY flag
    RCC->CFGR &= ~RCC_CFGR_SW;                 // reset system clock SWitch
    RCC->CFGR |= RCC_CFGR_SW_HSE;              // SWitch to HSE
    while ( !(RCC->CFGR & RCC_CFGR_SWS_HSE) ); // wait for SWitch Status to HSE

#ifdef USE_HYPERDRIVE
    RCC->CFGR |=  RCC_CFGR_PLLSRC; // set PLL SouRCe to HSE

#ifdef USE_LIGHT_SPEED
    RCC->CFGR  &= ~RCC_CFGR_PLLMULL;   // reset PLL MULtipLication
    RCC->CFGR  |= RCC_CFGR_PLLMULL9;   // x 9
    FLASH->ACR |= FLASH_ACR_LATENCY_2; // 2 wait states !!! REQUIRED !!!
    RCC->CFGR  |= RCC_CFGR_PPRE1_2;    // div SYSCLK by 2 == 72 MHz / 2 == 36 MHz
#endif /* USE_LIGHT_SPEED */

    RCC->CR   |=  RCC_CR_PLLON;                // set PLL ON (== enable)
    while ( !(RCC->CR & RCC_CR_PLLRDY) );      // wait for the PLLReaDY flag
    RCC->CFGR &= ~RCC_CFGR_SW;                 // reset system clock SWitch
    RCC->CFGR |= RCC_CFGR_SW_PLL;              // SWitch to PLL
    while ( !(RCC->CFGR & RCC_CFGR_SWS_PLL) ); // wait for SWitch Status to PLL
#endif /* USE_HYPERDRIVE */

    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= CFG_APB2EN;
    gpio_pin_init(ONBOARD_LED_GPIO, ONBOARD_LED_PIN, O02_GP_PP);
    ONBOARD_LED_GPIO->BRR = 1 << ONBOARD_LED_PIN; // PIN low, LED on

#ifdef USE_HYPERDRIVE
#ifdef USE_LIGHT_SPEED
    systick_delay_irq(SystemCoreClock / 1000 * 200); // 0.20 second
#else
    systick_delay_irq(SystemCoreClock / 2);          // 0.5 second
#endif /* USE_LIGHT_SPEED */
#else
    systick_delay_irq(SystemCoreClock);              // 1 second
#endif /* USE_HYPERDRIVE */

    return 0;
}

void
SysTick_Handler(void) {
#ifdef USE_HYPERDRIVE
#define EXPECTED_SWS    RCC_CFGR_SWS_PLL
#ifdef USE_LIGHT_SPEED
#define EXPECTED_CLOCK  (8000000 * 9) // HSE (8 MHz) * 9 (PLLMUL == 7 == PLL input clock x 9)
#else
#define EXPECTED_CLOCK  (8000000 * 2) // HSE (8 MHz) * 2 (PLLMUL == 0 == PLL input clock x 2)
#endif /* USE_LIGHT_SPEED */
#else
#define EXPECTED_SWS    RCC_CFGR_SWS_HSE
#define EXPECTED_CLOCK  8000000
#endif /* USE_HYPERDRIVE */
     if ((RCC->CFGR & RCC_CFGR_SWS) == EXPECTED_SWS && SystemCoreClock == EXPECTED_CLOCK) {
        ONBOARD_LED_GPIO->ODR ^= 1 << ONBOARD_LED_PIN; // toogle state
    }
}
