#include "stm32f1xx.h"
#include "gpio.h"
#include "systick.h"

#include "config.h"

enum STATES { INIT = 0, RED, ORANGE, GREEN };

unsigned int state, systick_irqs;

int
main (void) {
    SystemCoreClockUpdate();    // Update globale variable SystemCoreClock
    RCC->APB2ENR |= CFG_APB2EN; // Enable GPIOs

    gpio_pin_init(SWITCH_GPIO, SWITCH_PIN, O02_GP_PP);
    SWITCH_GPIO->BRR = 1 << SWITCH_PIN; // PIN low SWITCH off

    gpio_pin_init(RED_GPIO,    RED_PIN,    O02_GP_PP);
    gpio_pin_init(ORANGE_GPIO, ORANGE_PIN, O02_GP_PP);
    gpio_pin_init(GREEN_GPIO,  GREEN_PIN,  O02_GP_PP);

    // PINs high LEDs off
    RED_GPIO->BSRR    = 1 << RED_PIN;
    ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;
    GREEN_GPIO->BSRR  = 1 << GREEN_PIN;

    SWITCH_GPIO->BSRR = 1 << SWITCH_PIN; // PIN high SWITCH on

    state = INIT;
    systick_irqs = 0;
    systick_delay_irq(SystemCoreClock); // 1 second @8 MHz

    while (1) {
        __WFI();
        if (state == RED && systick_irqs == RED_SECONDS) {
            systick_irqs    = 0;
            state           = GREEN;
            RED_GPIO->BSRR  = 1 << RED_PIN;   // PIN high RED off
            GREEN_GPIO->BRR = 1 << GREEN_PIN; // PIN low GREEN on
        }
        else if (state == GREEN && systick_irqs == GREEN_SECONDS) {
            systick_irqs     = 0;
            state            = ORANGE;
            GREEN_GPIO->BSRR = 1 << GREEN_PIN;  // PIN high GREEN off
            ORANGE_GPIO->BRR = 1 << ORANGE_PIN; // PIN low ORANGE on
        }
        else if (state == ORANGE && systick_irqs == ORANGE_SECONDS) {
            systick_irqs      = 0;
            state             = RED;
            ORANGE_GPIO->BSRR = 1 << ORANGE_PIN; // PIN high ORANGE off
            RED_GPIO->BRR     = 1 << RED_PIN;    // PIN low RED on
        }
        else if (state == INIT && systick_irqs == INIT_SECONDS) {
            systick_irqs  = 0;
            state         = RED;
            RED_GPIO->BRR = 1 << RED_PIN; // PIN low RED on
        }
    }

    return 0;
}

void
SysTick_Handler(void) {
    systick_irqs++;
}
