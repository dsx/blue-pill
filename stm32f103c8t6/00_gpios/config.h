#pragma once

#define INIT_SECONDS  3

#define SWITCH_GPIO GPIOA
#define SWITCH_PIN  7

#define RED_GPIO     GPIOA
#define RED_PIN      3
#define RED_SECONDS  8

#define ORANGE_GPIO     GPIOA
#define ORANGE_PIN      4
#define ORANGE_SECONDS  4

#define GREEN_GPIO     GPIOA
#define GREEN_PIN      5
#define GREEN_SECONDS  8

#define CFG_APB2EN  RCC_APB2ENR_IOPAEN
