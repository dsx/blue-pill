#include "stm32f1xx.h"
#include "usart.h"

void
usart_putc(char c) {
    while (!(USART_USART->SR & USART_SR_TXE)); // wait for Transmit data register Empty
    USART_USART->DR = c;
}

void
usart_puts(const char * const buf) {
    const char *c = buf;

    while (*c) {
        usart_putc(*c++);
    }
    usart_putc('\r');
    usart_putc('\n');
}
