#pragma once

#define USART_USART      USART2
#define USART_APB1_EN    RCC_APB1ENR_USART2EN                     // usart 2 @APB1
#define USART_APB2_EN    RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN  // gpio + alternate function @APB2
#define USART_GPIO       GPIOA
#define USART_PIN_TX     2
#define USART_BAUD_RATE  9600

#ifdef USE_ONBOARD_LED
#define ONBOARD_LED_GPIO     GPIOC
#define ONBOARD_LED_PIN      13
#define ONBOARD_LED_APB2_EN  RCC_APB2ENR_IOPCEN
#endif
