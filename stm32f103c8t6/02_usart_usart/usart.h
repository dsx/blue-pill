#pragma once

#include "stm32f1xx.h"

#include "config.h"

void usart_putc(char c);
void usart_puts(const char * const buf);
