#include "stm32f1xx.h"
#include "clock.h"
#include "gpio.h"
#include "systick.h"
#include "usart.h"

#include "config.h"

int
main (void) {
    uint16_t usart_div_value;
    clock_hse();             // 8 MHz
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB1ENR |= USART_APB1_EN;                         // Enable USART 2
#ifdef USE_ONBOARD_LED
    RCC->APB2ENR |= (USART_APB2_EN | ONBOARD_LED_APB2_EN); // Enable GPIOs & Alternate Function
#else
    RCC->APB2ENR |= USART_APB2_EN;                         // Enable GPIOs & Alternate Function
#endif

#ifdef USE_ONBOARD_LED
    gpio_pin_init(ONBOARD_LED_GPIO, ONBOARD_LED_PIN, O02_GP_PP);
#endif
    gpio_pin_init(USART_GPIO, USART_PIN_TX, O02_AF_PP); // Alternate Function, max speed 02 MHz

    usart_div_value   = SystemCoreClock / USART_BAUD_RATE; // 8 MHz / 9600
    USART_USART->BRR  = ( ((usart_div_value / 16) << 4) | ((usart_div_value % 16) << 0) );
    USART_USART->CR1 |= (USART_CR1_TE | USART_CR1_UE);     // enable Transmission & Usart

    systick_delay_irq(SystemCoreClock); // 1 s @8 MHz

    while (1) {
        __WFI();
#ifdef USE_ONBOARD_LED
        ONBOARD_LED_GPIO->BRR  = 1 << ONBOARD_LED_PIN; // PIN low, LED on
#endif
        usart_puts("Hello world !");
#ifdef USE_ONBOARD_LED
        ONBOARD_LED_GPIO->BSRR = 1 << ONBOARD_LED_PIN; // PIN high, LED off
#endif
    }

    return 0;
}

void
SysTick_Handler(void) {
// nothing to do
}
