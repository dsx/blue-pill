#include "stm32f1xx.h"
#include "clock.h"

#ifdef USE_CLOCK_HSE
void
clock_hse(void) {
    RCC->CR   |= RCC_CR_HSEON;                   // set High Speed External ON (== enable)
    while ( !(RCC->CR & RCC_CR_HSERDY) );        // wait for High Speed External ReaDY flag
    RCC->CFGR &= ~RCC_CFGR_SW;                   // reset system clock SWitch
    RCC->CFGR |=  RCC_CFGR_SW_HSE;               // SWitch to HSE
    while ( !(RCC->CFGR & RCC_CFGR_SWS_HSE) );   // wait for SWitch Status to HSE
    RCC->CR   &= ~(RCC_CR_HSION | RCC_CR_PLLON); // disable HSI & PLL
}
#endif

#ifdef USE_CLOCK_HSI
void
clock_hsi(void) {
    RCC->CR   |= RCC_CR_HSION;                   // set High Speed Internal ON (== enable)
    while ( !(RCC->CR & RCC_CR_HSIRDY) );        // wait for High Speed Internal ReaDY flag
    RCC->CFGR &= ~RCC_CFGR_SW;                   // reset system clock SWitch == switch to HSI
    while (RCC->CFGR & RCC_CFGR_SWS_HSI);        // wait for SWitch Status to HSI
    RCC->CR   &= ~(RCC_CR_HSEON | RCC_CR_PLLON); // disable HSE & PLL
}
#endif

#ifdef USE_CLOCK_PLL

#define CLOCK_PRESCALERS RCC_CFGR_PLLXTPRE | RCC_CFGR_ADCPRE | RCC_CFGR_PPRE2 | RCC_CFGR_PPRE1 | RCC_CFGR_HPRE

void
clock_pll(uint32_t pll_mull, uint32_t prescalers) {
    RCC->CFGR  |=  RCC_CFGR_PLLSRC;  // set PLL SouRCe to HSE
    RCC->CFGR  &= ~RCC_CFGR_PLLMULL; // reset PLL MULtipLication
    RCC->CFGR  |=  pll_mull;         // set PLL multiplication

    FLASH->ACR &= ~FLASH_ACR_LATENCY;          // reset flash latency
    if (pll_mull > RCC_CFGR_PLLMULL6) {
        FLASH->ACR |= FLASH_ACR_LATENCY_2;     // 2 wait states !!! REQUIRED !!!
    }
    else if (pll_mull > RCC_CFGR_PLLMULL3) {
        FLASH->ACR |= FLASH_ACR_LATENCY_1;     // 1 wait state !!! REQUIRED !!!
    }

    RCC->CFGR &= ~(CLOCK_PRESCALERS); // reset prescalers
    RCC->CFGR |= prescalers;          // set prescalers

    RCC->CR   |=  RCC_CR_PLLON;                // set PLL ON (== enable)
    while ( !(RCC->CR & RCC_CR_PLLRDY) );      // wait for the PLLReaDY flag
    RCC->CFGR &= ~RCC_CFGR_SW;                 // reset system clock SWitch
    RCC->CFGR |=  RCC_CFGR_SW_PLL;             // SWitch to PLL
    while ( !(RCC->CFGR & RCC_CFGR_SWS_PLL) ); // wait for SWitch Status to PLL
    RCC->CR   &= ~(RCC_CR_HSION);              // disable HSI
}
#endif
