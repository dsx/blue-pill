#include "stm32f1xx.h"
#include "gpio.h"
#include "ic_74hc595.h"

void
ic_74hc595_init(IC_74HC595_TypeDef *ic_74hc595, enum GPIO_PIN_SETTINGS settings) {
    gpio_pins_init(ic_74hc595->gpio, settings, ic_74hc595->pins, IC_74HC595_PINS_SIZE);
}

void
ic_74hc595_send(IC_74HC595_TypeDef *ic_74hc595, uint8_t byte) {
    ic_74hc595->gpio->BRR = (1 << ic_74hc595->pins[IC_74HC595_RCLK]);         // pin low
    for (uint8_t i = 0; i < 8; i++) {
        ic_74hc595->gpio->BRR  = (1 << ic_74hc595->pins[IC_74HC595_SRCLK]);   // pin low
        if ((byte >> i) & 0x01)
            ic_74hc595->gpio->BSRR = (1 << ic_74hc595->pins[IC_74HC595_SER]); // 1: pin high
        else
            ic_74hc595->gpio->BRR = (1 << ic_74hc595->pins[IC_74HC595_SER]);  // 0: pin low
        ic_74hc595->gpio->BSRR = (1 << ic_74hc595->pins[IC_74HC595_SRCLK]);   // pin high
    }
    ic_74hc595->gpio->BSRR = (1 << ic_74hc595->pins[IC_74HC595_RCLK]);        // pin high
}
