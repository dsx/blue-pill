#pragma once

/*
   - A -
   |   |
   F   B
   |   |
   - G -
   |   |
   E   C
   |   |
   - D -  .H
*/

const uint8_t digit2segments[] = {
    //ABCDEFGdp
    0b11111100,
    0b01100000,
    0b11011010,
    0b11110010,
    0b01100110,
    0b10110110,
    0b10111110,
    0b11100000,
    0b11111110,
    0b11110110,
};
