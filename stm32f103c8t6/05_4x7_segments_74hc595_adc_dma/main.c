#include "stm32f1xx.h"
#include "adc.h"
#include "clock.h"
#include "digit2segments.h"
#include "gpio.h"
#include "ic_74hc595.h"
#include "systick.h"

#include "config.h"

#define SENSORS_ADC  ADC1  // Only ADC1 and ADC3 can generate a DMA request
#define SENSORS_DMA_CCR_SIZE  DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 // MSIZE & PSIZE 16 bits
#define SENSORS_DMA_CCR_MODE  DMA_CCR_MINC | DMA_CCR_CIRC       // Memory INCrement & CIRCular

#ifndef SENSORS_CHANNELS
#error "SENSORS_CHANNELS not defined"
#endif

#define ADC_MAX_VALUE  4096  // 2^12

uint16_t adc_values[] = { SENSORS_CHANNELS }; // channel number as fake value

void
send_value(IC_74HC595_TypeDef *digits, IC_74HC595_TypeDef *segments, uint8_t digit2value[], uint32_t value, uint32_t load) {
    uint8_t digit, i;

    for (i = 0; i < 4; i++) {
        digit = value % 10;
        ic_74hc595_send(digits, digit2value[i]);
        ic_74hc595_send(segments, digit2segments[digit]);
        systick_delay_load(load);
        value /= 10;
#ifdef USE_TRIM_VALUE
        if (!value) { return; }
#endif
    }
}

int
main (void) {
    uint32_t load;
    uint16_t adc_value, current_value = 0;
    uint8_t digit2value[] = { 0xFF & ~(DIGIT0_VALUE), 0xFF & ~(DIGIT1_VALUE), 0xFF & ~(DIGIT2_VALUE), 0xFF & ~(DIGIT3_VALUE) }; // common cathode, select digit if low
    int8_t sensors_channels[] = { SENSORS_CHANNELS, -1 };
    uint8_t i;

    IC_74HC595_TypeDef digits = {
        .gpio = DIGITS_GPIO,
        .pins = { DIGITS_SER_PIN, DIGITS_RCLK_PIN, DIGITS_SRCLK_PIN }
    };

    IC_74HC595_TypeDef segments = {
        .gpio = SEGMENTS_GPIO,
        .pins = { SEGMENTS_SER_PIN, SEGMENTS_RCLK_PIN, SEGMENTS_SRCLK_PIN }
    };

    clock_hse(); // 8 MHz
#ifdef USE_CLOCK_PLL
#  ifdef USE_CLOCK_PLL_MULL
    clock_pll(USE_CLOCK_PLL_MULL, 0);
#  else
    clock_pll(RCC_CFGR_PLLMULL2, 0);
#  endif
#endif
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= CFG_APB2EN | ADC_APB2EN; // Enable GPIOs & ADC

    ic_74hc595_init(&digits, O02_GP_PP);
    ic_74hc595_init(&segments, O02_GP_PP);

    adc_init(SENSORS_ADC, SENSORS_CR2);
    adc_init_calibration(SENSORS_ADC);
    for (i = 0; sensors_channels[i] != -1; i++) {
        adc_regular_add(SENSORS_ADC, sensors_channels[i], ADC_239_5, i);
        adc_values[i] = 0; // Reset initial values
    }
    SENSORS_ADC->SQR1 |= (i - 1) << ADC_SQR1_L_Pos;  // Sequence length (3 < i)
    SENSORS_ADC->CR1  |= ADC_CR1_SCAN;               // Scan mode
    SENSORS_ADC->CR2  |= ADC_CR2_DMA | ADC_CR2_CONT; // Enable DMA & CONTinous mode

    RCC->AHBENR          |= RCC_AHBENR_DMA1EN;      // DMA1 ENable
    DMA1_Channel1->CCR   |= SENSORS_DMA_CCR_SIZE | SENSORS_DMA_CCR_MODE;
    DMA1_Channel1->CMAR   = (uint32_t)&adc_values;  // Set Channel Memory Address Register to adc values address
    DMA1_Channel1->CPAR   = (uint32_t)&(ADC1->DR);  // Set Channel Peripheral Address Register to adc Data Register address
    DMA1_Channel1->CNDTR  = i;                      // Sizeof adc_values[]
    DMA1_Channel1->CCR   |= DMA_CCR_EN;             // Enable DMA Channel 1

    load = SystemCoreClock / PERSISTENCE_OF_VISION;

    SENSORS_ADC->CR2   |= ADC_CR2_ADON;
    SENSORS_ADC->CR2   |= ADC_CR2_SWSTART;

    while (1) {
        adc_value = 0;
    	for (i = 0; sensors_channels[i] != -1; i++) {
            adc_value += adc_values[i];
        }
#define DIFF_MIN  30
        if (((current_value - adc_value) < -DIFF_MIN) || ((current_value - adc_value) > DIFF_MIN)) { current_value = adc_value; }
        send_value(&digits, &segments, digit2value, current_value / i, load);
    }

    return 0;
}
