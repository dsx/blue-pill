#include "stm32f1xx.h"
#include "adc.h"
#include "gpio.h"

#define ADC_BITS_PER_CYCLE    3
#define ADC_CYCLE_RESET_Msk   0x7
#define ADC_ADC_PRESCALER     (RCC->CFGR & RCC_CFGR_ADCPRE_Msk)
#define ADC_TWO_CLOCK_CYCLES  2

void adc_init_gpio(uint8_t channel);

void
adc_init(ADC_TypeDef *adc, uint32_t cr2) {
    uint8_t cycles;
    adc->CR2 |= cr2;
    adc->CR2 |= ADC_CR2_ADON; // wake up
    // P.223: Before starting a calibration, the ADC must have been in power-on state (ADON bit = ‘1’) for at least two ADC clock cycles.
    cycles = ADC_TWO_CLOCK_CYCLES * ((1 + ADC_ADC_PRESCALER) * 2); // SysClk != AdcClk
    while (cycles--) {                                             // more than 2 ADC clock cycles
        __NOP();
    }
    adc->CR2 |= ADC_CR2_CAL;
    while (adc->CR2 & ADC_CR2_CAL);
}

void
adc_init_channel(ADC_TypeDef *adc, uint8_t channel, enum ADC_CYCLES cycle) {
    volatile uint32_t *smpr = channel < 10 ? &adc->SMPR2 : &adc->SMPR1;

    if (channel < CHANNEL_TEMPERATURE_SENSOR) {
        adc_init_gpio(channel);
    }
    else {
        adc->CR2 |= ADC_CR2_TSVREFE; // Temperature Sensor and VREFint Enable
    }
    if (channel > 9) { channel -= 10; }
    channel *= ADC_BITS_PER_CYCLE;

    *smpr &= ~(ADC_CYCLE_RESET_Msk << channel);
    *smpr |= cycle << channel;
}

void
adc_init_gpio(uint8_t channel) {
    if (channel < 8)       { gpio_pin_init(GPIOA, channel,      I_ANALOG); } //  0 ..  7
    else if (channel < 10) { gpio_pin_init(GPIOB, channel -  8, I_ANALOG); } //  8 .. 9
    else                   { gpio_pin_init(GPIOC, channel - 10, I_ANALOG); } // 10 .. 15
}

#ifdef USE_ADC_READ
uint16_t
adc_read(ADC_TypeDef *adc) {
/*
Note: If any other bit in this register apart from ADON is changed at the same time, then
conversion is not triggered. This is to prevent triggering an erroneous conversion.
    adc->CR2 |= ADC_CR2_SWSTART | ADC_CR2_ADON; // => Fail !!!
*/
    adc->CR2 |= ADC_CR2_ADON;
    adc->CR2 |= ADC_CR2_SWSTART;
    while (!(adc->SR & ADC_SR_EOC)); // wait for End Of Conversion

    return (uint16_t)adc->DR;
}
#endif
