#include "stm32f1xx.h"
#include "adc.h"
#include "clock.h"
#include "gpio.h"
#ifdef USE_SWO
#include "swo.h"
#endif
#include "systick.h"

#include "config.h"

int
main (void) {
    uint16_t adc_value;
    clock_hse(); // 8 MHz
#ifdef USE_ADC_HIGH_SPEED
    clock_pll(RCC_CFGR_PLLMULL7, RCC_CFGR_ADCPRE_DIV4); // 56 MHz, ADC 14 MHz
#else
    clock_pll(RCC_CFGR_PLLMULL3, 0);                    // 24 MHz, ADC 12 MHz
#endif
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

#ifdef USE_SWO
    RCC->APB2ENR |= SWO_APB2EN | ADC_APB2EN | CFG_APB2EN;
#else
    RCC->APB2ENR |= ADC_APB2EN | CFG_APB2EN;
#endif

    gpio_pin_init(SWITCH_GPIO, SWITCH_PIN, O02_GP_PP);
    SWITCH_GPIO->BRR = 1 << SWITCH_PIN; // PIN low SWITCH off

    gpio_pin_init(RED_GPIO,    RED_PIN,    O02_GP_PP);
    gpio_pin_init(ORANGE_GPIO, ORANGE_PIN, O02_GP_PP);
    gpio_pin_init(GREEN_GPIO,  GREEN_PIN,  O02_GP_PP);

    adc_init(POTENTIOMETER_ADC, POTENTIOMETER_CR2);
    adc_init_channel(POTENTIOMETER_ADC, POTENTIOMETER_CHANNEL, ADC_239_5);
    POTENTIOMETER_ADC->SQR3 |= POTENTIOMETER_CHANNEL << 0;

    // PINs high LEDs off
    RED_GPIO->BSRR    = 1 << RED_PIN;
    ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;
    GREEN_GPIO->BSRR  = 1 << GREEN_PIN;

    SWITCH_GPIO->BSRR = 1 << SWITCH_PIN; // PIN high SWITCH on
#ifdef USE_ADC_HIGH_SPEED
    systick_delay_irq(SystemCoreClock / 4); // 0.25 s @56 MHz
#else
    systick_delay_irq(SystemCoreClock / 4); // 0.25 s @24 MHz
#endif
#ifdef USE_SWO
    swo_init();
    swo_puts("start adc");
#endif

    while (1) {
        __WFI();
        adc_value = adc_read(POTENTIOMETER_ADC);
#ifdef USE_SWO
        swo_printf("%d\n", adc_value);
#endif

        RED_GPIO->BSRR    = 1 << RED_PIN;
        ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;
        GREEN_GPIO->BSRR  = 1 << GREEN_PIN;
        if (adc_value < GREEN_VALUE)       { GREEN_GPIO->BRR  = 1 << GREEN_PIN;  } // PIN low GREEN on
        else if (adc_value < ORANGE_VALUE) { ORANGE_GPIO->BRR = 1 << ORANGE_PIN; } // PIN low ORANGE on
        else                               { RED_GPIO->BRR    = 1 << RED_PIN;    } // PIN low RED on
    }

    return 0;
}

void
SysTick_Handler(void) {
//nothing to do
}
