#pragma once

#include "stm32f1xx.h"

#define SWITCH_GPIO  GPIOB
#define SWITCH_PIN   15

#define RED_GPIO  GPIOB
#define RED_PIN   12

#define ORANGE_GPIO   GPIOB
#define ORANGE_PIN    13
#define ORANGE_VALUE  3000

#define GREEN_GPIO   GPIOB
#define GREEN_PIN    14
#define GREEN_VALUE  1000

#define POTENTIOMETER_ADC      ADC1
#define POTENTIOMETER_CHANNEL  1
#define POTENTIOMETER_CR2      ADC_CR2_EXTSEL_2 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_0 // 111: External event select for regular group == SoftWareSTART

#define CFG_APB2EN  RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN // GPIO A: potentiometer, GPIO B: leds + transistor
