#pragma once

#include "stm32f1xx.h"

#define TM1637_CMD_DATA  0x40
#define TM1637_CMD_CTRL  0x80
#define TM1637_CMD_ADDR  0xC0

#define TM1637_CTRL_DISPLAY_OFF  0x00
#define TM1637_CTRL_DISPLAY_ON   0x08

#define TM1637_DATA_AUTO_INC  0x00
#define TM1637_DATA_FIXED     0x04

/*
[ 1 2: 3 4 ] (pins)

   - 0 -
   |   |
   5   1
   |   |
   - 6 -
   |   |
   4   2
   |   |
   - 3 -  .7
*/

typedef struct {
    GPIO_TypeDef *gpio;
    uint8_t      pin;
} tm1637_pin;

typedef struct {
    tm1637_pin clk;
    tm1637_pin dio;
} tm1637_t;

void tm1637_init(tm1637_t *tm1637, GPIO_TypeDef *clk_gpio, uint8_t clk_pin, GPIO_TypeDef *dio_gpio, uint8_t dio_pin);
void tm1637_stop(tm1637_t *tm1637);
void tm1637_send_cmd(tm1637_t *tm1637, uint8_t cmd);
void tm1637_send_data(tm1637_t *tm1637, uint8_t address, uint8_t data);
void tm1637_send_datas(tm1637_t *tm1637, uint8_t address, const uint8_t *datas, uint8_t size);
