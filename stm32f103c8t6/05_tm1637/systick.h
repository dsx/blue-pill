#pragma once

#if !USE_SYSTICK_DELAY_IRQ && !USE_SYSTICK_DELAY_LOAD && !USE_SYSTICK_DELAY_MS
#error "USE_SYSTICK_DELAY not defined"
#endif

#if USE_SYSTICK_DELAY_IRQ && (USE_SYSTICK_DELAY_LOAD || USE_SYSTICK_DELAY_MS)
#error "USE_SYSTICK_DELAY_IRQ defined, can't use another USE_SYSTICK_DELAY"
#endif

#if USE_SYSTICK_DELAY_IRQ
void systick_delay_irq(uint32_t load);
#endif

#if USE_SYSTICK_DELAY_LOAD
void systick_delay_load(uint32_t load);
#endif

#if USE_SYSTICK_DELAY_MS
void systick_delay_ms(uint32_t millis);
#endif
