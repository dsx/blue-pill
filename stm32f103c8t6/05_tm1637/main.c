#include "stm32f1xx.h"
#include "clock.h"
#include "systick.h"
#include "tm1637.h"
#ifdef USE_TM1637_FIXED
#include "tm1637_segments.h"
#endif
#ifdef USE_ONBOARD_LED
#include "../onboard_led.h"
#endif

#include "config.h"

int
main (void) {
    uint32_t load;
    uint8_t i;
#ifdef USE_TM1637_BLINK
    uint8_t brightness = 0;
#endif
    tm1637_t tm1637;

    clock_hse(); // 8 MHz
#ifdef USE_CLOCK_PLL
#if USE_CLOCK_PLL_MULL < 2 || USE_CLOCK_PLL_MULL > 9
#error "USE_CLOCK_PLL_MULL must be between 2 and 9"
#else
    clock_pll((USE_CLOCK_PLL_MULL - 2) << RCC_CFGR_PLLMULL_Pos, 0);
#endif
#endif
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock
    load = SystemCoreClock / 5;

#ifdef USE_ONBOARD_LED
    if (SystemCoreClock == 72000000) ONBOARD_LED_ENABLE;
#endif

    RCC->APB2ENR |= TM1637_RCC_APB2EN;
    tm1637_init(&tm1637, TM1637_CLK_GPIO, TM1637_CLK_PIN, TM1637_DIO_GPIO, TM1637_DIO_PIN);

    tm1637_send_cmd(&tm1637, TM1637_CMD_CTRL | TM1637_CTRL_DISPLAY_ON); // minimal brightness
    tm1637_stop(&tm1637);

#ifdef USE_TM1637_AUTO
    tm1637_send_cmd(&tm1637, TM1637_CMD_DATA | TM1637_DATA_AUTO_INC);
    tm1637_stop(&tm1637);
#endif
#ifdef USE_TM1637_BLINK
    tm1637_send_cmd(&tm1637, TM1637_CMD_DATA | TM1637_DATA_AUTO_INC);
    tm1637_stop(&tm1637);
    tm1637_send_datas(&tm1637, 0, fbsd, 4);
#endif
#ifdef USE_TM1637_FIXED
    tm1637_send_cmd(&tm1637, TM1637_CMD_DATA | TM1637_DATA_FIXED);
    tm1637_stop(&tm1637);
#endif

    while (1) {

#ifdef USE_ONBOARD_LED
    if (SystemCoreClock == 72000000) ONBOARD_LED_TOGGLE;
#endif

#ifdef USE_TM1637_AUTO
#if TM1637_ANIM0_SIZE
        for (i = 0; i < TM1637_ANIM0_SIZE; i++) {
            tm1637_send_datas(&tm1637, 0, anim0[i], 4);
            systick_delay_load(load);
        }
#endif
#if TM1637_ANIM1_SIZE
        for (i = 0; i < TM1637_ANIM1_SIZE; i++) {
            tm1637_send_datas(&tm1637, 0, anim1[i], 4);
            systick_delay_load(load);
        }
#endif
#endif // USE_TM1637_AUTO
#ifdef USE_TM1637_BLINK
        tm1637_send_cmd(&tm1637, TM1637_CMD_CTRL | TM1637_CTRL_DISPLAY_ON | brightness++ % 8);
        tm1637_stop(&tm1637);
        systick_delay_load(load);
        tm1637_send_cmd(&tm1637, TM1637_CMD_CTRL | TM1637_CTRL_DISPLAY_OFF);
        tm1637_stop(&tm1637);
        systick_delay_load(load);
#endif
#ifdef USE_TM1637_FIXED
        for (i = 0; i < 16; i++) {
           tm1637_send_data(&tm1637, i % 4, digit2segments[i]);
           if (i % 4 == 3) {
               systick_delay_load(load);
           }
        }
#endif
    }
    return 0;
}
