#include "stm32f1xx.h"
#include "gpio.h"
#include "tm1637.h"
#ifdef USE_TM1637_SYSTICK
#include "systick.h"
#endif

/*
@08 MHz  7 __NOP() OK
@16 MHz 14 __NOP() OK

for (...)
@08MHz  1 OK
@16MHz  7 OK
@24MHz 14 OK
...
*/

#ifdef USE_CLOCK_PLL_MULL
#define TM1637_MAX_DELAY  7 * USE_CLOCK_PLL_MULL
#else
#define TM1637_MAX_DELAY  1
#endif

#define TM1637_FAKE_DELAY     for (uint8_t tm1637_delay = TM1637_MAX_DELAY; tm1637_delay; tm1637_delay--) { __NOP(); }
#define TM1637_SYSTICK_DELAY  systick_delay_load(TM1637_MAX_DELAY);

#ifndef USE_TM1637_SYSTICK
#define TM1637_DELAY  TM1637_FAKE_DELAY
#else
#define TM1637_DELAY  TM1637_SYSTICK_DELAY
#endif

#define TM1637_CLK_HIGH  do { tm1637->clk.gpio->BSRR = 1 << tm1637->clk.pin; TM1637_DELAY } while (0);
#define TM1637_CLK_LOW   do { tm1637->clk.gpio->BRR  = 1 << tm1637->clk.pin; TM1637_DELAY } while (0);
#define TM1637_DIO_HIGH  do { tm1637->dio.gpio->BSRR = 1 << tm1637->dio.pin; TM1637_DELAY } while (0);
#define TM1637_DIO_LOW   do { tm1637->dio.gpio->BRR  = 1 << tm1637->dio.pin; TM1637_DELAY } while (0);

void tm1637_send_byte(tm1637_t *tm1637, uint8_t byte);
void tm1637_start(tm1637_t *tm1637);
static inline void tm1637_read_ack(tm1637_t *tm1637);

void
tm1637_init(tm1637_t *tm1637, GPIO_TypeDef *clk_gpio, uint8_t clk_pin, GPIO_TypeDef *dio_gpio, uint8_t dio_pin) {
    tm1637->clk.gpio = clk_gpio;
    tm1637->clk.pin  = clk_pin;
    tm1637->dio.gpio = dio_gpio;
    tm1637->dio.pin  = dio_pin;
    gpio_pin_init(tm1637->clk.gpio, tm1637->clk.pin, O02_GP_OD);
    gpio_pin_init(tm1637->dio.gpio, tm1637->dio.pin, O02_GP_OD);
    tm1637->clk.gpio->BSRR = 1 << tm1637->clk.pin;
    tm1637->dio.gpio->BSRR = 1 << tm1637->dio.pin;
    TM1637_CLK_HIGH;
    TM1637_DIO_HIGH;
}

static inline void
tm1637_read_ack(tm1637_t *tm1637) {
    TM1637_CLK_LOW;
    //gpio_pin_init(tm1637->clk.gpio, tm1637->clk.pin, I_PU_PD);
    // read ack ?
    TM1637_CLK_HIGH;
    TM1637_CLK_LOW;
    //gpio_pin_init(tm1637->clk.gpio, tm1637->clk.pin, O02_GP_OD);
}

void
tm1637_send_byte(tm1637_t *tm1637, uint8_t byte) {
    uint8_t i;

    for (i = 0; i < 8; i++) {
        TM1637_CLK_LOW;
        if (byte & 1) {
            TM1637_DIO_HIGH;
        }
        else {
            TM1637_DIO_LOW;
        }
        TM1637_CLK_HIGH;
        byte >>= 1;
    }
    tm1637_read_ack(tm1637);
}

void
tm1637_send_cmd(tm1637_t *tm1637, uint8_t cmd) {
    tm1637_start(tm1637);
    tm1637_send_byte(tm1637, cmd);
}

void
tm1637_send_data(tm1637_t *tm1637, uint8_t address, uint8_t data) { // Write SRAM data in a fixed address mode
    tm1637_send_cmd(tm1637, TM1637_CMD_ADDR | address);
    tm1637_send_byte(tm1637, data);
    tm1637_stop(tm1637);
}

void
tm1637_send_datas(tm1637_t *tm1637, uint8_t address, const uint8_t *datas, uint8_t size) { // Write SRAM data in address auto increment 1 mode
    uint8_t i;

    tm1637_send_cmd(tm1637, TM1637_CMD_ADDR | address);
    for (i = 0; i < size; i++) {
        tm1637_send_byte(tm1637, datas[i]);
    }
    tm1637_stop(tm1637);
}

void
tm1637_start(tm1637_t *tm1637) {
    TM1637_DIO_LOW;
    TM1637_CLK_LOW;
}

void
tm1637_stop(tm1637_t *tm1637) {
    TM1637_DIO_LOW;
    TM1637_CLK_HIGH;
    TM1637_DIO_HIGH;
}
