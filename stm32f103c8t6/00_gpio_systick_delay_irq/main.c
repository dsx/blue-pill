#include "stm32f1xx.h"
#include "systick.h"

#define UINT24_MAX 16777216

int
main (void) {
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN; // Enable GPIO C 
    GPIOC->CRH   |= GPIO_CRH_MODE13_1;  // GPIO C PIN 13 mode 0b10: Output mode, max speed 2 MHz

    systick_delay_irq(UINT24_MAX - 1);

    return 0;
}

void
SysTick_Handler(void) {
    GPIOC->ODR ^= GPIO_ODR_ODR13; // GPIO C PIN 13 output toggle state
}

/*

4.0 mA (led off) => 5.9 mA (led on)

*/
