#include "stm32f1xx.h"
#include "systick.h"

#if USE_SYSTICK_DELAY_LOAD
#define DELAY_1_MS  (SystemCoreClock / 1000)
#endif

#define DELAY_LED  250

int
main (void) {
#if USE_SYSTICK_DELAY_LOAD
    uint32_t load;
#endif
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock
#if USE_SYSTICK_DELAY_LOAD
    load = DELAY_LED * DELAY_1_MS; // OK @ 8 MHz
#endif

    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN; // Enable GPIO C 
    GPIOC->CRH   |= GPIO_CRH_MODE13_1;  // GPIO C PIN 13 mode 0b10: Output mode, max speed 2 MHz

    while (1) {
#if USE_SYSTICK_DELAY_LOAD
        systick_delay_load(load);
#elif USE_SYSTICK_DELAY_MS
        systick_delay_ms(DELAY_LED);
#endif
        GPIOC->ODR ^= GPIO_ODR_ODR13; // GPIO C PIN 13 output toggle state
    }

    return 0;
}

/*

DELAY_LED 2 000

USE_SYSTICK_DELAY_LOAD

4.5 mA (led off) => 6.4 mA (led on)

USE_SYSTICK_DELAY_MS

4.7 mA (led off) => 6.6 mA (led on)

*/
