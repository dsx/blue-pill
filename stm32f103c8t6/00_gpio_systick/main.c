#include "stm32f1xx.h"

#define DELAY_1_MS  (SystemCoreClock / 1000)
#define DELAY_LED   250    

int
main (void) {
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN; // Enable GPIO C 
    GPIOC->CRH   |= GPIO_CRH_MODE13_1;  // GPIO C PIN 13 mode 0b10: Output mode, max speed 2 MHz

    // https://developer.arm.com/documentation/dui0552/a/cortex-m3-peripherals/system-timer--systick/systick-control-and-status-register
    SysTick->LOAD = (DELAY_LED * DELAY_1_MS) - 1;                          // Countdown from (250 * (SystemCoreClock / 1000)) - 1 to zero (== 250 ms)
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;  // Set processor clock as clock source + Enable SysTick

    while (1) {
        while ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0); // 0 reached
        GPIOC->ODR ^= GPIO_ODR_ODR13;                              // GPIO C PIN 13 output toggle state
    }

    return 0;
}
