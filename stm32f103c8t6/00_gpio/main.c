#include "stm32f1xx.h"

int
main (void) {
    // Passe à 1 le bit numéro 4 du registre APB2 == activation du GPIO C
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
    // mode de la PIN 13 de GPIO C à 0b10 == Output mode, max speed 2 MHz
    GPIOC->CRH   |= GPIO_CRH_MODE13_1;
    // Passe à 1 le bit numéro 13 du registre BSRR == passage à l'étact haut/actif de la PIN 13 du GPIO C
    GPIOC->BSRR   = GPIO_BSRR_BS13;

    // La LED est maintenant éteinte
    return 0;
}
