#ifdef USE_USART_PRINTF
#include <stdarg.h>
#endif
#include "stm32f1xx.h"
#include "gpio.h"
#include "usart.h"

#ifdef USE_USART_PRINTF
#define MAX_DIGITS 10
char usart_printf_uint_buf[MAX_DIGITS];
void usart_printf_uint(unsigned int number);
#endif /* USE_USART_PRINTF */

#ifdef USE_USART_TX_DMA
char usart_tx_dma_buf[USART_TX_DMA_BUF_SIZE];
#if USART_TX_DMA_BUF_SIZE < 255
uint8_t usart_tx_dma_buf_len;
#else
uint16_t usart_tx_dma_buf_len;
#endif

void usart_tx_dma(void);
void usart_tx_dma_reset(void);
#endif /* USE_USART_TX_DMA */

void
usart_init(uint16_t usart_div_value) {
#ifdef USART_APB1_EN
    RCC->APB1ENR     |= USART_APB1_EN;                    // Enable USART[23]
#endif
    RCC->APB2ENR     |= USART_APB2_EN;                    // Enable [USART1 &] GPIOs & Alternate Function
#ifdef USE_USART1_REMAP
    AFIO->MAPR       |= AFIO_MAPR_USART1_REMAP;           // Remap USART1 *after* USART
#endif
    USART_USART->BRR  = ( ((usart_div_value / 16) << 4) | ((usart_div_value % 16) << 0) );
    USART_USART->CR1 |= (USART_CR1_TE | USART_CR1_UE);    // enable Transmission & Usart

    gpio_pin_init(USART_GPIO, USART_PIN_TX, O02_AF_PP);
#ifdef USE_USART_TX_DMA
    __disable_irq();
    USART_USART->CR3          |= USART_CR3_DMAT;                            // DMA enable Transmitter
    RCC->AHBENR               |= RCC_AHBENR_DMA1EN;                         // DMA1 ENable
    USART_TX_DMA_CHANNEL->CCR |= DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE; // Memory INCrement mode & data transfer DIRection (Read from memory) & Transfer Complete Interrupt Enable
    NVIC_EnableIRQ(USART_TX_DMA_IRQn);
    USART_TX_DMA_CHANNEL->CMAR = (uint32_t)usart_tx_dma_buf;                // set Channel Memory Address Register to TX buffer address
    USART_TX_DMA_CHANNEL->CPAR = (uint32_t)&USART_USART->DR;                // set Channel Peripheral Address Register to usart Data Register address
    __enable_irq();
#endif /* USE_USART_TX_DMA */
}

#ifdef USE_USART_PRINTF
void
usart_printf(const char * const fmt, ...) {
    va_list ap;
    const char *c = fmt;

    va_start(ap, fmt);

    while (*c) {
        if (*c != '%') {
            usart_putc(*c++);
            continue;
        }

        c++;
        if (!*c) { break; }

        switch (*c) {
        case '%':
            usart_putc('%');
            break;
        case 'c':
            usart_putc(va_arg(ap, int));
            break;
        case 'd':
            usart_printf_uint(va_arg(ap, unsigned int));
            break;
        }
        c++;
    }

    va_end(ap);
#ifdef USE_USART_TX_DMA
    usart_tx_dma();
#endif
}

void
usart_printf_uint(unsigned int number) {
    uint8_t pos = 0;

    if (!number) {
        usart_putc('0');
        return;
    }

    while (number && pos < MAX_DIGITS) {
        usart_printf_uint_buf[pos++] = 48 + number % 10;
        number /= 10;
    }
    
    while (pos--) {
        usart_putc(usart_printf_uint_buf[pos]);
    }
}
#endif /* USE_USART_PRINTF */

void
usart_putc(char c) {
#ifdef USE_USART_TX_DMA
    usart_tx_dma_buf[usart_tx_dma_buf_len] = c;
    usart_tx_dma_buf_len++;
#else
    while (!(USART_USART->SR & USART_SR_TXE)); // wait for Transmit data register Empty
    USART_USART->DR = c;
#endif
}

void
usart_puts(const char * const buf) {
    const char *c = buf;

    while (*c) {
        usart_putc(*c++);
    }
    usart_putc('\r');
    usart_putc('\n');
#ifdef USE_USART_TX_DMA
    usart_tx_dma();
#endif
}

#ifdef USE_USART_TX_DMA
void
usart_tx_dma(void) {
    USART_TX_DMA_CHANNEL->CNDTR  = usart_tx_dma_buf_len; // set Channel Number of DaTa Register
    USART_TX_DMA_CHANNEL->CCR   |= DMA_CCR_EN;           // enable DMA Channel

    while (usart_tx_dma_buf_len); // wait for end of transfer
}

void
usart_tx_dma_reset(void) {
    usart_tx_dma_buf[0]        = '\0';                    // reset buffer
    usart_tx_dma_buf_len       = 0;                       // end of transfer
    USART_TX_DMA_CHANNEL->CCR &= ~DMA_CCR_EN;             // disable DMA Channel
    DMA1->IFCR                |= USART_TX_DMA_IFCR_CTCIF; // set Interrupt Flag Clear Register
}

#ifdef USE_USART3
void
DMA1_Channel2_IRQHandler(void) {
    if (DMA1->ISR & DMA_ISR_TCIF2) {
        usart_tx_dma_reset();
    }
}
#endif

#ifdef USE_USART1
void
DMA1_Channel4_IRQHandler(void) {
    if (DMA1->ISR & DMA_ISR_TCIF4) {
        usart_tx_dma_reset();
    }
}
#endif

#ifdef USE_USART1_REMAP
void
DMA1_Channel4_IRQHandler(void) {
    if (DMA1->ISR & DMA_ISR_TCIF4) {
        usart_tx_dma_reset();
    }
}
#endif

#ifdef USE_USART2
void
DMA1_Channel7_IRQHandler(void) {
    if (DMA1->ISR & DMA_ISR_TCIF7) {
        usart_tx_dma_reset();
    }
}
#endif
#endif /* USE_USART_TX_DMA */
