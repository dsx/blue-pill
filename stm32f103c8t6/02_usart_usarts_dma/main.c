#include "stm32f1xx.h"
#include "clock.h"
#include "systick.h"
#include "usart.h"

int
main (void) {
#ifdef USE_USART_TX_DMA
    int i = 0;
#endif
    clock_hse();             // 8 MHz
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    usart_init(SystemCoreClock / USART_BAUD_RATE);

    systick_delay_irq(SystemCoreClock); // 1 s @8 MHz

    while (1) {
        __WFI();
#ifdef USE_USART_TX_DMA
        usart_printf("Hello world from " USART_NAME " @%d + DMA %d !\r\n", USART_BAUD_RATE, i++);
#else
        usart_printf("Hello world from " USART_NAME " @%d !\r\n", USART_BAUD_RATE);
#endif
    }

    return 0;
}

void
SysTick_Handler(void) {
//nothing to do
}
