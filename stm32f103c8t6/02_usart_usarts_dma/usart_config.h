#pragma once

#include "stm32f1xx.h"

#if defined USE_USART1
#define USART_USART      USART1
#define USART_NAME       "USART1"
#define USART_APB2_EN    (RCC_APB2ENR_USART1EN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN)  // usart 1 + gpio A + alternate function @APB2
#define USART_GPIO       GPIOA
#define USART_PIN_TX     9
#ifdef USE_USART_TX_DMA
#define USART_TX_DMA_CHANNEL     DMA1_Channel4
#define USART_TX_DMA_IRQn        DMA1_Channel4_IRQn
#define USART_TX_DMA_IFCR_CTCIF  DMA_IFCR_CTCIF4
#endif /* USE_USART_TX_DMA */

#elif defined USE_USART1_REMAP
#define USART_USART      USART1
#define USART_NAME       "USART1r"
#define USART_APB2_EN    (RCC_APB2ENR_USART1EN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN)  // usart 1 + gpio B + alternate function @APB2
#define USART_GPIO       GPIOB
#define USART_PIN_TX     6
#ifdef USE_USART_TX_DMA
#define USART_TX_DMA_CHANNEL     DMA1_Channel4
#define USART_TX_DMA_IRQn        DMA1_Channel4_IRQn
#define USART_TX_DMA_IFCR_CTCIF  DMA_IFCR_CTCIF4
#endif /* USE_USART_TX_DMA */

#elif defined USE_USART2
#define USART_USART      USART2
#define USART_NAME       "USART2"
#define USART_APB1_EN    RCC_APB1ENR_USART2EN                       // usart 2 @APB1
#define USART_APB2_EN    (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN)  // gpio A + alternate function @APB2
#define USART_GPIO       GPIOA
#define USART_PIN_TX     2
#ifdef USE_USART_TX_DMA
#define USART_TX_DMA_CHANNEL     DMA1_Channel7
#define USART_TX_DMA_IRQn        DMA1_Channel7_IRQn
#define USART_TX_DMA_IFCR_CTCIF  DMA_IFCR_CTCIF7
#endif /* USE_USART_TX_DMA */

#elif defined USE_USART3
#define USART_USART      USART3
#define USART_NAME       "USART3"
#define USART_APB1_EN    RCC_APB1ENR_USART3EN                       // usart 3 @APB1
#define USART_APB2_EN    (RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN)  // gpio B + alternate function @APB2
#define USART_GPIO       GPIOB
#define USART_PIN_TX     10
#ifdef USE_USART_TX_DMA
#define USART_TX_DMA_CHANNEL     DMA1_Channel2
#define USART_TX_DMA_IRQn        DMA1_Channel2_IRQn
#define USART_TX_DMA_IFCR_CTCIF  DMA_IFCR_CTCIF2
#endif /* USE_USART_TX_DMA */

#else
#error "no USE_USART defined"
#endif /* USE_USART? */

#ifdef USE_USART_TX_DMA
#ifndef USART_TX_DMA_BUF_SIZE
#define USART_TX_DMA_BUF_SIZE  128
#endif
#endif /* USE_USART_TX_DMA */
