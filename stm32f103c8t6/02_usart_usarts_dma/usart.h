#pragma once

#include "stm32f1xx.h"
#include "usart_config.h"

void usart_init(uint16_t usart_div_value);
#ifdef USE_USART_PRINTF
void usart_printf(const char * const fmt, ...);
#endif
void usart_putc(char c);
void usart_puts(const char * const buf);
