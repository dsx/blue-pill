#include "stm32f1xx.h"
#include "clock.h"
#include "swo.h"
#include "systick.h"

int
main (void) {
    clock_hse();             // 8 MHz
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= SWO_APB2EN;
    swo_init();
    systick_delay_irq(SystemCoreClock); // 1 s @8 MHz

    while (1) {
        __WFI();
       swo_puts("Hello world !");
    }

    return 0;
}

void
SysTick_Handler(void) {
//nothing to do
}
