#pragma once

#ifdef USE_CLOCK_PLL
#define USE_CLOCK_HSE 1
#endif

#ifdef USE_CLOCK_HSE
void clock_hse(void);
#endif
#ifdef USE_CLOCK_HSI
void clock_hsi(void);
#endif
#ifdef USE_CLOCK_PLL
void clock_pll(uint32_t pll_mull, uint32_t prescalers);
#endif
