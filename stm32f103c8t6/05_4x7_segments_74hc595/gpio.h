#pragma once

#include "stm32f1xx.h"

enum GPIO_PIN_SETTINGS {
    I_ANALOG,   // 00:00 Analog mode : Input mode
    O10_GP_PP,  // 00:01 General purpose output push-pull : Output mode, max speed 10 MHz
    O02_GP_PP,  // 00:10 General purpose output push-pull : Output mode, max speed 02 MHz
    O50_GP_PP,  // 00:11 General purpose output push-pull : Output mode, max speed 50 MHz
    I_FLOATING, // 01:00 Floating input : Input mode (reset state)
    O10_GP_OD,  // 01:01 General purpose output open-drain : Output mode, max speed 10 MHz
    O02_GP_OD,  // 01:10 General purpose output open-drain : Output mode, max speed 02 MHz
    O50_GP_OD,  // 01:11 General purpose output open-drain : Output mode, max speed 50 MHz
    I_PU_PD,    // 10:00 Input with pull-up / pull-down : Input mode
    O10_AF_PP,  // 10:01 Alternate function output push-pull : Output mode, max speed 10 MHz
    O02_AF_PP,  // 10:10 Alternate function output push-pull : Output mode, max speed 02 MHz
    O50_AF_PP,  // 10:11 Alternate function output push-pull : Output mode, max speed 50 MHz
    RESERVED,   // 11:00 Reserved : Input mode
    O10_AF_OD,  // 11:01 Alternate function output push-pull : Output mode, max speed 10 MHz
    O02_AF_OD,  // 11:10 Alternate function output push-pull : Output mode, max speed 02 MHz
    O50_AF_OD,  // 11:11 Alternate function output push-pull : Output mode, max speed 50 MHz
};

void gpio_pin_init(GPIO_TypeDef *gpio, uint8_t pin, enum GPIO_PIN_SETTINGS settings);
void gpio_pins_init(GPIO_TypeDef *gpio, enum GPIO_PIN_SETTINGS settings, uint8_t pins[], uint8_t size);
