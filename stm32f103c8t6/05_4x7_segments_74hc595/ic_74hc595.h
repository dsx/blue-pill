#pragma once

#include "stm32f1xx.h"
#include "gpio.h"

enum IC_74HC595_PINS {
    IC_74HC595_SER,   // 14 DS
    IC_74HC595_RCLK,  // 12 ST_CP
    IC_74HC595_SRCLK, // 11 SH_CP
    IC_74HC595_PINS_SIZE
};

typedef struct {
    GPIO_TypeDef *gpio;
    uint8_t pins[IC_74HC595_PINS_SIZE];
} IC_74HC595_TypeDef;

void ic_74hc595_init(IC_74HC595_TypeDef *ic_74hc595, enum GPIO_PIN_SETTINGS settings);
void ic_74hc595_send(IC_74HC595_TypeDef *ic_74hc595, uint8_t byte);
