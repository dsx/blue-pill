#include "stm32f1xx.h"
#include "systick.h"

#if USE_SYSTICK_DELAY_IRQ
void
systick_delay_irq(uint32_t load) {
    SysTick->LOAD = load;
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
}
#endif /* USE_SYSTICK_DELAY_IRQ */

#if USE_SYSTICK_DELAY_LOAD
void
systick_delay_load(uint32_t load) {
    SysTick->LOAD  = load;
    SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;

    while ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0);

    SysTick->CTRL = 0;
}
#endif /* USE_SYSTICK_DELAY_LOAD */

#if USE_SYSTICK_DELAY_MS
#define SYSTICK_DELAY_1_MS  (SystemCoreClock / 1000)

void
systick_delay_ms(uint32_t millis) {
    SysTick->LOAD  = SYSTICK_DELAY_1_MS - 1;
    SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;

    while (millis--) {
        while ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0);
    }

    SysTick->CTRL = 0;
}
#endif /* USE_SYSTICK_DELAY_MS */
