#include "stm32f1xx.h"
#include "clock.h"
#include "digit2segments.h"
#include "gpio.h"
#include "ic_74hc595.h"
#include "systick.h"

#include "config.h"

void
send_value(IC_74HC595_TypeDef *digits, IC_74HC595_TypeDef *segments, uint8_t digit2value[], uint32_t value, uint32_t load) {
    uint8_t digit, i;

    for (i = 0; i < 4; i++) {
        digit = value % 10;
        ic_74hc595_send(digits, digit2value[i]);
        ic_74hc595_send(segments, digit2segments[digit]);
        systick_delay_load(load);
        value /= 10;
#ifdef USE_TRIM_VALUE
        if (!value) { return; }
#endif
    }
}

int
main (void) {
    uint32_t load;
    uint8_t digit2value[] = { 0xFF & ~(DIGIT0_VALUE), 0xFF & ~(DIGIT1_VALUE), 0xFF & ~(DIGIT2_VALUE), 0xFF & ~(DIGIT3_VALUE) }; // common cathode, select digit if low

    IC_74HC595_TypeDef digits = {
        .gpio = DIGITS_GPIO,
        .pins = { DIGITS_SER_PIN, DIGITS_RCLK_PIN, DIGITS_SRCLK_PIN }
    };

    IC_74HC595_TypeDef segments = {
        .gpio = SEGMENTS_GPIO,
        .pins = { SEGMENTS_SER_PIN, SEGMENTS_RCLK_PIN, SEGMENTS_SRCLK_PIN }
    };

    clock_hse(); // 8 MHz
#ifdef USE_CLOCK_PLL
#  ifdef USE_CLOCK_PLL_MULL
    clock_pll(USE_CLOCK_PLL_MULL, 0);
#  else
    clock_pll(RCC_CFGR_PLLMULL2, 0); // 16 MHz
#  endif
#endif
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= CFG_APB2EN; // Enable GPIOs

    ic_74hc595_init(&digits, O02_GP_PP);
    ic_74hc595_init(&segments, O02_GP_PP);

    load = SystemCoreClock / PERSISTENCE_OF_VISION;
    while (1) {
        send_value(&digits, &segments, digit2value, 42, load);
    }

    return 0;
}
