#include "stm32f1xx.h"
#include "gpio.h"

#define GPIO_BITS_PER_PIN   4
#define GPIO_PIN_RESET_Msk  0xf
#define GPIO_HIGH_OFFSET    32 // 8 * GPIO_BITS_PER_PIN

void
gpio_pin_init(GPIO_TypeDef *gpio, uint8_t pin, enum GPIO_PIN_SETTINGS settings) {
    pin *= GPIO_BITS_PER_PIN;
    if (pin < GPIO_HIGH_OFFSET) {
        gpio->CRL &= ~(GPIO_PIN_RESET_Msk << pin);
        gpio->CRL |= settings << pin;
    }
    else {
        pin -= GPIO_HIGH_OFFSET;
        gpio->CRH &= ~(GPIO_PIN_RESET_Msk << pin);
        gpio->CRH |= settings << pin;
    }
}

void
gpio_pins_init(GPIO_TypeDef *gpio, enum GPIO_PIN_SETTINGS settings, uint8_t pins[], uint8_t size) {
    do {
        size--;
        gpio_pin_init(gpio, pins[size], settings);
    } while (size);
}
