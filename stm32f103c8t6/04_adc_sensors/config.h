#pragma once

#include "stm32f1xx.h"

#define SWITCH_GPIO  GPIOB
#define SWITCH_PIN   15

#define RED_GPIO  GPIOB
#define RED_PIN   12

#define ORANGE_GPIO   GPIOB
#define ORANGE_PIN    13
#define ORANGE_VALUE  3000

#define GREEN_GPIO   GPIOB
#define GREEN_PIN    14
#define GREEN_VALUE  1000

#define ONBOARD_LED_GPIO  GPIOC
#define ONBOARD_LED_PIN   13

#define BUTTON_GPIO  GPIOB
#define BUTTON_PIN   11

#define BUTTON_EXTICR_IDX  (BUTTON_PIN / 4)
#define BUTTON_EXTICR_VAL  AFIO_EXTICR3_EXTI11_PB  // 3 == 1 + (BUTTON_PIN / 4), 11 == BUTTON_PIN, B == BUTTON GPIO
#define BUTTON_EXTI_PR     EXTI_PR_PR11            // 11 == BUTTON_PIN
#define BUTTON_FTSR        EXTI_FTSR_TR11          // F == Falling, 11 == BUTTON_PIN
#define BUTTON_HANDLER     EXTI15_10_IRQHandler    // cf STM32-base-master/startup/STM32F1xx/STM32F103xB.s
#define BUTTON_IMR         EXTI_IMR_MR11           // 11 == BUTTON_PIN
#define BUTTON_IRQn        EXTI15_10_IRQn          // 15_10 == BUTTON_PIN

#define SENSORS_CR2  ADC_CR2_EXTSEL_2 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_0 // 111: External event select for regular group == SoftWareSTART

#define SENSORS_CHANNELS  1, 2, 3  // potentiometer, light dependant resistor, potentiometer

#define CFG_APB2EN  RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN // GPIO A: sensors, GPIO B: leds + transistor/switch + button, GPIO C: onboard led
