#include "stm32f1xx.h"
#include "adc.h"
#include "gpio.h"

void adc_init_gpio(uint8_t channel);

void
adc_init(ADC_TypeDef *adc, uint32_t cr2) {
    adc->CR2 |= cr2;
    adc->CR2 |= ADC_CR2_ADON; // wake up
}

void
adc_init_calibration(ADC_TypeDef *adc) {
    adc->CR2 |= ADC_CR2_CAL;
    while (adc->CR2 & ADC_CR2_CAL);
}

void
adc_init_channel(ADC_TypeDef *adc, uint8_t channel, enum ADC_CYCLES cycle) {
    volatile uint32_t *smpr = channel < 10 ? &adc->SMPR2 : &adc->SMPR1;

    if (channel < CHANNEL_TEMPERATURE_SENSOR) {
        adc_init_gpio(channel);
    }
    else {
        adc->CR2 |= ADC_CR2_TSVREFE; // Temperature Sensor and VREFint Enable
    }
#define BITS_PER_CYCLE  3
#define RESET_3BITS     0x7
    if (channel > 9) { channel -= 10; }
    channel *= BITS_PER_CYCLE;

    *smpr &= ~(RESET_3BITS << channel);
    *smpr |= cycle << channel;
}

void
adc_init_gpio(uint8_t channel) {
    if (channel < 8)       { gpio_pin_init(GPIOA, channel,      I_ANALOG); } //  0 ..  7
    else if (channel < 10) { gpio_pin_init(GPIOB, channel -  8, I_ANALOG); } //  8 .. 9
    else                   { gpio_pin_init(GPIOC, channel - 10, I_ANALOG); } // 10 .. 15
}

#ifdef USE_ADC_READ
uint16_t
adc_read(ADC_TypeDef *adc) {
/*
Note: If any other bit in this register apart from ADON is changed at the same time, then
conversion is not triggered. This is to prevent triggering an erroneous conversion.
    adc->CR2 |= ADC_CR2_SWSTART | ADC_CR2_ADON; => BAD
*/
    adc->CR2 |= ADC_CR2_ADON;
    adc->CR2 |= ADC_CR2_SWSTART;
    while (!(adc->SR & ADC_SR_EOC)); // wait for End Of Conversion

    return (uint16_t)adc->DR;
}
#endif

#ifdef USE_ADC_MULTI_CHANNELS
void
adc_regular_add(ADC_TypeDef *adc, uint8_t channel, enum ADC_CYCLES cycles, uint8_t idx) {
    volatile uint32_t *sqr;
    uint8_t sqr_pos;

    adc_init_channel(adc, channel, cycles);
#define BITS_PER_SEQUENCE  5
#define RESET_5BITS        0x1F
    if (idx < 6) {
        sqr     = &adc->SQR3;
        sqr_pos = idx * BITS_PER_SEQUENCE;
    }
    else if (idx < 12) {
        sqr     = &adc->SQR2;
        sqr_pos = (idx - 6) * BITS_PER_SEQUENCE;
    }
    else {
        sqr     = &adc->SQR1;
        sqr_pos = (idx - 12) * BITS_PER_SEQUENCE;
    }
    *sqr &= ~(RESET_5BITS << sqr_pos);
    *sqr |= channel << sqr_pos;
}
#endif
