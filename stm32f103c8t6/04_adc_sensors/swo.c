#ifdef USE_SWO_PRINTF
#include <stdarg.h>
#endif
#include "stm32f1xx.h"
#include "gpio.h"
#include "swo.h"

#ifdef USE_SWO_PRINTF
#define MAX_DIGITS 10
char swo_printf_uint_buf[MAX_DIGITS];
void swo_printf_uint(unsigned int number);
#endif /* USE_SWO_PRINTF */

void
swo_init(void) {
    gpio_pin_init(SWO_GPIO, SWO_PIN, O02_AF_PP);
}

void
swo_puts(const char * const buf) {
    const char *c = buf;

    while (*c) {
        ITM_SendChar(*c++);
    }
    ITM_SendChar('\r');
    ITM_SendChar('\n');
}

#ifdef USE_SWO_PRINTF
void
swo_printf(const char * const fmt, ...) {
    va_list ap;
    const char *c = fmt;

    va_start(ap, fmt);

    while (*c) {
        if (*c != '%') {
            ITM_SendChar(*c++);
            continue;
        }

        c++;
        if (!*c) { break; }

        switch (*c) {
        case '%':
            ITM_SendChar('%');
            break;
        case 'c':
            ITM_SendChar(va_arg(ap, int));
            break;
        case 'd':
            swo_printf_uint(va_arg(ap, unsigned int));
            break;
        }
        c++;
    }

    va_end(ap);
}

void
swo_printf_uint(unsigned int number) {
    uint8_t pos = 0;

    if (!number) {
        ITM_SendChar('0');
        return;
    }

    while (number && pos < MAX_DIGITS) {
        swo_printf_uint_buf[pos++] = 48 + number % 10;
        number /= 10;
    }
    
    while (pos--) {
        ITM_SendChar(swo_printf_uint_buf[pos]);
    }
}
#endif /* USE_SWO_PRINTF */
