#pragma once

#define SWO_GPIO    GPIOB
#define SWO_PIN     3
#define SWO_APB2EN  RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN

void swo_init(void);
#ifdef USE_SWO_PRINTF
void swo_printf(const char * const fmt, ...);
#endif
void swo_puts(const char * const buf);
