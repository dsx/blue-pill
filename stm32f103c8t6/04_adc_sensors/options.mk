# normal speed
#SWO_SPEED = 24000000

# high speed
#CFLAGS    += -DUSE_ADC_HIGH_SPEED
#SWO_SPEED  = 56000000

.ifdef SWO_SPEED
include ../swo.mk
.endif
