#include "stm32f1xx.h"
#include "adc.h"
#include "clock.h"
#include "gpio.h"
#include "systick.h"

#include "config.h"

#ifdef SWO_SPEED
#include "swo.h"
#endif

#define SENSORS_ADC  ADC1  // Only ADC1 and ADC3 can generate a DMA request
#define SENSORS_DMA_CCR_SIZE  DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 // MSIZE & PSIZE 16 bits
#define SENSORS_DMA_CCR_MODE  DMA_CCR_MINC | DMA_CCR_CIRC       // Memory INCrement & CIRCular

#ifndef SENSORS_CHANNELS
#error "SENSORS_CHANNELS not defined"
#endif

#define ADC_MAX_VALUE         4096  // 2^12
#define SYSTICK_LOAD_MAX  16000000  // 0.625s @24 MHz
#define SYSTICK_LOAD_MIN   1200000  // 0.050s @24 MHz
#define SYSTICK_LOAD_STEP  ((SYSTICK_LOAD_MAX - SYSTICK_LOAD_MIN) / ADC_MAX_VALUE)

uint16_t adc_values[] = { SENSORS_CHANNELS }; // channel number as fake value
uint8_t i, current_sensor;
GPIO_TypeDef *sensors_led_gpio[] = { RED_GPIO, ORANGE_GPIO, GREEN_GPIO };
uint8_t sensors_led_pin[]        = { RED_PIN,  ORANGE_PIN,  GREEN_PIN  };

int
main (void) {
    int8_t sensors_channels[] = { SENSORS_CHANNELS, -1 };

    clock_hse(); // 8 MHz
#ifdef USE_ADC_HIGH_SPEED
    clock_pll(RCC_CFGR_PLLMULL7, RCC_CFGR_ADCPRE_DIV4); // 56 MHz, ADC 14 MHz
#else
    clock_pll(RCC_CFGR_PLLMULL3, 0);                    // 24 MHz, ADC 12 MHz
#endif
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= CFG_APB2EN | ADC_APB2EN; // enable GPIOs & ADC1

    gpio_pin_init(SWITCH_GPIO, SWITCH_PIN, O02_GP_PP);
    SWITCH_GPIO->BRR = 1 << SWITCH_PIN; // PIN low SWITCH off

    gpio_pin_init(RED_GPIO,    RED_PIN,    O02_GP_PP);
    gpio_pin_init(ORANGE_GPIO, ORANGE_PIN, O02_GP_PP);
    gpio_pin_init(GREEN_GPIO,  GREEN_PIN,  O02_GP_PP);
    gpio_pin_init(ONBOARD_LED_GPIO, ONBOARD_LED_PIN, O02_GP_PP); // LED on

    // PINs high LEDs off
    RED_GPIO->BSRR    = 1 << RED_PIN;
    ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;
    GREEN_GPIO->BSRR  = 1 << GREEN_PIN;

    __disable_irq();
    AFIO->EXTICR[BUTTON_EXTICR_IDX] = BUTTON_EXTICR_VAL;
    EXTI->IMR  |= BUTTON_IMR;
    EXTI->FTSR |= BUTTON_FTSR;
    NVIC_EnableIRQ(BUTTON_IRQn);
    __enable_irq();

    adc_init(SENSORS_ADC, SENSORS_CR2);
    adc_init_calibration(SENSORS_ADC);
    for (i = 0; sensors_channels[i] != -1; i++) {
        adc_regular_add(SENSORS_ADC, sensors_channels[i], ADC_239_5, i);
        adc_values[i] = ADC_MAX_VALUE / 2; // reset initial values
    }
    SENSORS_ADC->SQR1 |= (i - 1) << ADC_SQR1_L_Pos;  // sequence length (3 < i)
    SENSORS_ADC->CR1  |= ADC_CR1_SCAN;               // scan mode
    SENSORS_ADC->CR2  |= ADC_CR2_DMA | ADC_CR2_CONT; // enable DMA & CONTinous mode

    RCC->AHBENR          |= RCC_AHBENR_DMA1EN;      // DMA1 ENable
    DMA1_Channel1->CCR   |= SENSORS_DMA_CCR_SIZE | SENSORS_DMA_CCR_MODE;
    DMA1_Channel1->CMAR   = (uint32_t)&adc_values;  // set Channel Memory Address Register to adc values address
    DMA1_Channel1->CPAR   = (uint32_t)&(ADC1->DR);  // set Channel Peripheral Address Register to adc Data Register address
    DMA1_Channel1->CNDTR  = i;                      // Sizeof adc_values[]
    DMA1_Channel1->CCR   |= DMA_CCR_EN;             // Enable DMA Channel 1

    SWITCH_GPIO->BSRR = 1 << SWITCH_PIN; // PIN high SWITCH on
#ifdef USE_ADC_HIGH_SPEED
    systick_delay_irq(SystemCoreClock / 5); // 0.25 s @56 MHz
#else
    systick_delay_irq(SystemCoreClock / 2); // 0.50 s @24 MHz
#endif

    SENSORS_ADC->CR2   |= ADC_CR2_ADON;
    SENSORS_ADC->CR2   |= ADC_CR2_SWSTART;

    current_sensor = 0;
    sensors_led_gpio[current_sensor]->BRR = 1 << sensors_led_pin[current_sensor]; // PIN low, LED on

#ifdef SWO_SPEED
    swo_init();

    while(1) {
        __WFI();
        swo_printf("%d %d %d\n", adc_values[0], adc_values[1], adc_values[2]);
    }
#endif

    return 0;
}

void
SysTick_Handler(void) {
    ONBOARD_LED_GPIO->ODR ^= 1 << ONBOARD_LED_PIN; // toggle state
    SysTick->LOAD = SYSTICK_LOAD_MIN + (adc_values[current_sensor] * SYSTICK_LOAD_STEP);
}

void
BUTTON_HANDLER(void) {
    EXTI->PR |= BUTTON_EXTI_PR; // Reset irq
    sensors_led_gpio[current_sensor]->BSRR = 1 << sensors_led_pin[current_sensor]; // PIN high, LED off
    current_sensor++;
    if (current_sensor == i) { current_sensor = 0; }
    sensors_led_gpio[current_sensor]->BRR = 1 << sensors_led_pin[current_sensor]; // PIN low, LED on
}
