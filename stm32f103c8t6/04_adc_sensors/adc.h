#pragma once

#include "stm32f1xx.h"

/*

PA0 -> ADC12_IN0
PA1 -> ADC12_IN1
PA2 -> ADC12_IN2
PA3 -> ADC12_IN3
PA4 -> ADC12_IN4
PA5 -> ADC12_IN5
PA6 -> ADC12_IN6
PA7 -> ADC12_IN7
PB0 -> ADC12_IN8
PB1 -> ADC12_IN9
PC0 -> ADC12_IN10
PC1 -> ADC12_IN11
PC2 -> ADC12_IN12
PC3 -> ADC12_IN13
PC4 -> ADC12_IN14
PC5 -> ADC12_IN15

ADC12_IN16 Temperature sensor
ADC12_IN17 Reference voltage

*/

#define CHANNEL_TEMPERATURE_SENSOR  16
#define CHANNEL_REFERENCE_VOLTAGE   17

#ifdef USE_ADC2
#define ADC_APB2EN  RCC_APB2ENR_ADC2EN | RCC_APB2ENR_ADC1EN | RCC_APB2ENR_AFIOEN
#else
#define ADC_APB2EN  RCC_APB2ENR_ADC1EN | RCC_APB2ENR_AFIOEN
#endif

enum ADC_CYCLES {
    ADC_1_5, ADC_7_7, ADC_13_5, ADC_28_5, ADC_41_5, ADC_55_5, ADC_71_5, ADC_239_5
};

void adc_init(ADC_TypeDef *adc, uint32_t cr2);
void adc_init_calibration(ADC_TypeDef *adc);
void adc_init_channel(ADC_TypeDef *adc, uint8_t channel, enum ADC_CYCLES cycles);
#ifdef USE_ADC_READ
uint16_t adc_read(ADC_TypeDef *adc);
#endif
#ifdef USE_ADC_MULTI_CHANNELS
void adc_regular_add(ADC_TypeDef *adc, uint8_t channel, enum ADC_CYCLES cycles, uint8_t idx);
#endif
