#include "stm32f1xx.h"
#include "systick.h"

#define UINT24_MAX    16777216
#define SYSTICK_IRQS  4

int systick_irqs;

int
main (void) {
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN; // Enable GPIO C 
    GPIOC->CRH   |= GPIO_CRH_MODE13_1;  // GPIO C PIN 13 mode 0b10: Output mode, max speed 2 MHz

    systick_irqs = 0;
    systick_delay_irq(UINT24_MAX - 1);

    while (1) {
        __WFI();
        if (systick_irqs == SYSTICK_IRQS) {
            systick_irqs = 0;
            GPIOC->ODR ^= GPIO_ODR_ODR13; // GPIO C PIN 13 output toggle state
        }
    }

    return 0;
}

void
SysTick_Handler(void) {
    systick_irqs++;
}

/*

Without __WFI

4.90 mA (led off) => 6.82 mA (led on)

With __WFI

3.0 mA (led off) => 4.92 mA (led on)

*/
