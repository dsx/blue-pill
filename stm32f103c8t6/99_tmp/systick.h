#pragma once

#if !USE_SYSTICK_DELAY_LOAD && !USE_SYSTICK_DELAY_MS
#error "USE_SYSTICK_DELAY not defined"
#endif

#if USE_SYSTICK_DELAY_LOAD
void systick_delay_load(uint32_t load);
#endif

#if USE_SYSTICK_DELAY_MS
void systick_delay_ms(uint32_t millis);
#endif
