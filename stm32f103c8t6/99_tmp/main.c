#include "stm32f1xx.h"
#include "clock.h"
#ifdef USE_SYSTICK_DELAY_LOAD
#include "systick.h"
#endif

int
main (void) {
    clock_hse(); // 8 MHz [ 5.07 - 5.08 ]
#if defined USE_CLOCK_16
    clock_pll(RCC_CFGR_PLLMULL2, 0); // 16 MHz [ 7.56 - 7.58 ]
#elif defined USE_CLOCK_24
    clock_pll(RCC_CFGR_PLLMULL3, 0); // 24 MHz [ 9.96 - 9.98 ]
#elif defined USE_CLOCK_32
    clock_pll(RCC_CFGR_PLLMULL4, 0); // 32 MHz [ 8.46 - 8.48 ]
#elif defined USE_CLOCK_36
    clock_pll(RCC_CFGR_PLLMULL9,  RCC_CFGR_PLLXTPRE_HSE_DIV2); // 36 MHz [ 9.16 - 9.17 ]
#elif defined USE_CLOCK_40
    clock_pll(RCC_CFGR_PLLMULL5, 0); // 40 MHz [ 9.87 - 9.89 ]
#elif defined USE_CLOCK_48
    clock_pll(RCC_CFGR_PLLMULL6, 0); // 48 MHz [ 11.29 - 11.31 ]
#elif defined USE_CLOCK_48_2
    clock_pll(RCC_CFGR_PLLMULL12, RCC_CFGR_PLLXTPRE_HSE_DIV2); // 48 MHz [ 11.28 - 11.30 ]
#elif defined USE_CLOCK_56
    clock_pll(RCC_CFGR_PLLMULL7, 0); // 56 MHz [ 12.72 - 12.74 ]
#elif defined USE_CLOCK_64
    clock_pll(RCC_CFGR_PLLMULL8, 0); // 64 MHz [ 14.13 - 14.16 ]
#elif defined USE_CLOCK_72
    clock_pll(RCC_CFGR_PLLMULL9, 0); // 72 MHz [ 15.58 - 15.61 ]
#endif

#ifdef USE_SYSTICK_DELAY_LOAD
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN; // Enable GPIO C 
    GPIOC->CRH   |= GPIO_CRH_MODE13_1;  // GPIO C PIN 13 mode 0b10: Output mode, max speed 2 MHz

    while (1) {
        systick_delay_load(16000000);
        GPIOC->ODR ^= GPIO_ODR_ODR13; // GPIO C PIN 13 output toggle state
    }
#endif

    return 0;
}
