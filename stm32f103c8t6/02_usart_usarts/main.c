#include "stm32f1xx.h"
#include "clock.h"
#include "systick.h"
#include "usart.h"

#ifndef USE_USART_PRINTF
#define xstr(s) str(s)
#define str(s)  #s
#endif

int
main (void) {
    clock_hse();             // 8 MHz
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock

    usart_init(SystemCoreClock / USART_BAUD_RATE);

    systick_delay_irq(SystemCoreClock); // 1 s @8 MHz

    while (1) {
        __WFI();
#ifdef USE_USART_PRINTF
	usart_printf("Hello world from " USART_NAME " @%d !\r\n", USART_BAUD_RATE);
#else
        usart_puts("Hello world from " USART_NAME " @" xstr(USART_BAUD_RATE) " !");
#endif
    }

    return 0;
}

void
SysTick_Handler(void) {
// nothing to do
}
