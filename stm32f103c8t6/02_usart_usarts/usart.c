#ifdef USE_USART_PRINTF
#include <stdarg.h>
#endif
#include "stm32f1xx.h"
#include "gpio.h"
#include "usart.h"

#ifdef USE_USART_PRINTF
#define MAX_DIGITS 10
char buf[MAX_DIGITS];
void usart_printf_uint(unsigned int number);
#endif

void
usart_init(uint16_t usart_div_value) {
#ifdef USART_APB1_EN
    RCC->APB1ENR     |= USART_APB1_EN;                    // Enable USART[23]
#endif
    RCC->APB2ENR     |= USART_APB2_EN;                    // Enable [USART1 &] GPIOs & Alternate Function
#ifdef USE_USART1_REMAP
    AFIO->MAPR       |= AFIO_MAPR_USART1_REMAP;           // Remap USART1 *after* USART
#endif
    USART_USART->BRR  = ( ((usart_div_value / 16) << 4) | ((usart_div_value % 16) << 0) );
    USART_USART->CR1 |= (USART_CR1_TE | USART_CR1_UE);    // enable Transmission & Usart

    gpio_pin_init(USART_GPIO, USART_PIN_TX, O02_AF_PP);
}

#ifdef USE_USART_PRINTF
void
usart_printf(const char * const fmt, ...) {
    va_list ap;
    const char *c = fmt;

    va_start(ap, fmt);

    while (*c) {
        if (*c != '%') {
            usart_putc(*c++);
            continue;
        }

        c++;
        if (!*c) { break; }

        switch (*c) {
        case '%':
            usart_putc('%');
            break;
        case 'c':
            usart_putc(va_arg(ap, int));
            break;
        case 'd':
            usart_printf_uint(va_arg(ap, unsigned int));
            break;
        }
        c++;
    }

    va_end(ap);
}

void
usart_printf_uint(unsigned int number) {
    uint8_t pos = 0;

    if (!number) {
        usart_putc('0');
        return;
    }

    while (number && pos < MAX_DIGITS) {
        buf[pos++] = 48 + number % 10;
        number /= 10;
    }
    
    while (pos--) {
        usart_putc(buf[pos]);
    }
}
#endif

void
usart_putc(char c) {
    while (!(USART_USART->SR & USART_SR_TXE)); // wait for Transmit data register Empty
    USART_USART->DR = c;
}

void
usart_puts(const char * const buf) {
    const char *c = buf;

    while (*c) {
        usart_putc(*c++);
    }
    usart_putc('\r');
    usart_putc('\n');
}
