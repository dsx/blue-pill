#include "stm32f1xx.h"
#include "clock.h"
#include "gpio.h"
#include "systick.h"

#include "config.h"

enum STATES { RED = 0, ORANGE, GREEN }; // HSI, HSE, PLL

int current_state, previous_state;
#ifdef NB_SECONDS
int seconds;
#endif

int
main (void) {
    SystemCoreClockUpdate();    // Update globale variable SystemCoreClock
    RCC->APB2ENR |= CFG_APB2EN; // Enable GPIOs

    gpio_pin_init(SWITCH_GPIO, SWITCH_PIN, O02_GP_PP);
    SWITCH_GPIO->BRR = 1 << SWITCH_PIN; // PIN low SWITCH off

    gpio_pin_init(RED_GPIO,    RED_PIN,    O02_GP_PP);
    gpio_pin_init(ORANGE_GPIO, ORANGE_PIN, O02_GP_PP);
    gpio_pin_init(GREEN_GPIO,  GREEN_PIN,  O02_GP_PP);
    gpio_pin_init(BUTTON_GPIO, BUTTON_PIN, I_PU_PD);

    gpio_pin_init(ONBOARD_LED_GPIO, ONBOARD_LED_PIN, O02_GP_PP);
    ONBOARD_LED_GPIO->BSRR = 1 << ONBOARD_LED_PIN; // PIN high, LED off

    // PINs high LEDs off
    RED_GPIO->BSRR    = 1 << RED_PIN;
    ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;
    GREEN_GPIO->BSRR  = 1 << GREEN_PIN;

    SWITCH_GPIO->BSRR = 1 << SWITCH_PIN; // PIN high SWITCH on

    __disable_irq();
    AFIO->EXTICR[BUTTON_EXTICR_IDX] = BUTTON_EXTICR_VAL;
    EXTI->IMR  |= BUTTON_IMR;
    EXTI->FTSR |= BUTTON_FTSR;
    NVIC_EnableIRQ(BUTTON_IRQn);
    __enable_irq();

    systick_delay_irq(SystemCoreClock); // 1 s @8 MHz
    RED_GPIO->BRR  = 1 << RED_PIN;      // PIN low RED on
    current_state  = RED;
    previous_state = RED;

#ifdef NB_SECONDS
    seconds = 0;
#endif

    while (1) {
        __WFI();
        if (current_state != previous_state) {
            switch (current_state) {
                case RED:
                    clock_hsi();                       // 8 MHz
                    GREEN_GPIO->BSRR = 1 << GREEN_PIN; // PIN high, LED off
                    RED_GPIO->BRR    = 1 << RED_PIN;   // PIN low, LED on
                    break;
                case ORANGE:
                    clock_hse();                        // 8 MHz
                    RED_GPIO->BSRR   = 1 << RED_PIN;    // PIN high, LED off
                    ORANGE_GPIO->BRR = 1 << ORANGE_PIN; // PIN low, LED on
                    break;
                case GREEN:
                    clock_pll(RCC_CFGR_PLLMULL2, RCC_CFGR_PLLXTPRE_HSE_DIV2); // 2 * (HSE / 2) == 8 MHz
                    ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;                      // PIN high, LED off
                    GREEN_GPIO->BRR   = 1 << GREEN_PIN;                       // PIN low, LED high
                    break;
                default: break;
            }
            previous_state = current_state;
        }

#ifdef NB_SECONDS
	if (seconds && seconds % NB_SECONDS == 0) {
            SWITCH_GPIO->ODR ^= 1 << SWITCH_PIN; // toogle state
            seconds = 0;
        }
#else
        if (RCC->CFGR & RCC_CFGR_SWS_PLL) {
            ONBOARD_LED_GPIO->BRR  = 1 << ONBOARD_LED_PIN; // on
        }
        else {
            ONBOARD_LED_GPIO->BSRR = 1 << ONBOARD_LED_PIN; // off
        }
#endif /* NB_SECONDS */
    }

    return 0;
}

void
SysTick_Handler(void) {
#ifdef NB_SECONDS
seconds++;
#else
// nothing to do
#endif
}

void
BUTTON_HANDLER(void) {
    EXTI->PR |= BUTTON_EXTI_PR; // Reset irq
    if (current_state == GREEN) {
        current_state = RED;
    }
    else {
        current_state++;
    }
}
