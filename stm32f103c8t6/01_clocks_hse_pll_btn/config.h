#pragma once

#define ONBOARD_LED_GPIO  GPIOC
#define ONBOARD_LED_PIN   13

#define SWITCH_GPIO  GPIOA
#define SWITCH_PIN   7

#define RED_GPIO  GPIOA
#define RED_PIN   3

#define ORANGE_GPIO  GPIOA
#define ORANGE_PIN   4

#define GREEN_GPIO  GPIOA
#define GREEN_PIN   5

#define BUTTON_GPIO  GPIOA
#define BUTTON_PIN   0

#define BUTTON_EXTICR_IDX  (BUTTON_PIN / 4)
#define BUTTON_EXTICR_VAL  AFIO_EXTICR1_EXTI0_PA  // 1 == 1 + (BUTTON_PIN / 4), 0 == BUTTON_PIN, A == BUTTON GPIO
#define BUTTON_EXTI_PR     EXTI_PR_PR0            // 0 == BUTTON_PIN
#define BUTTON_FTSR        EXTI_FTSR_TR0          // F == Falling, 0 == BUTTON_PIN
#define BUTTON_HANDLER     EXTI0_IRQHandler       // cf STM32-base-master/startup/STM32F1xx/STM32F103xB.s
#define BUTTON_IMR         EXTI_IMR_MR0           // 0 == BUTTON_PIN
#define BUTTON_IRQn        EXTI0_IRQn             // 0 == BUTTON_PIN

#define CFG_APB2EN  RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_AFIOEN // GPIOA + GPIOC + AFIO

//#define NB_SECONDS  4 // time to measure current consumption
