#include "stm32f1xx.h"
#include "gpio.h"
#include "systick.h"

#include "config.h"

enum STATES { INIT = 0, RED, ORANGE, GREEN, EMERGENCY };

unsigned int state, systick_irqs;

int
main (void) {
    SystemCoreClockUpdate(); // Update globale variable SystemCoreClock
    RCC->APB2ENR |= CFG_APB2EN; // Enable GPIOs

    gpio_pin_init(SWITCH_GPIO, SWITCH_PIN, O02_GP_PP);
    SWITCH_GPIO->BRR = 1 << SWITCH_PIN; // PIN low SWITCH off

    gpio_pin_init(RED_GPIO,    RED_PIN,    O02_GP_PP);
    gpio_pin_init(ORANGE_GPIO, ORANGE_PIN, O02_GP_PP);
    gpio_pin_init(GREEN_GPIO,  GREEN_PIN,  O02_GP_PP);
#ifdef USE_BUTTON_EMERGENCY
    gpio_pin_init(BUTTON_GPIO, BUTTON_PIN, I_PU_PD);
#endif

    // PINs high LEDs off
    RED_GPIO->BSRR    = 1 << RED_PIN;
    ORANGE_GPIO->BSRR = 1 << ORANGE_PIN;
    GREEN_GPIO->BSRR  = 1 << GREEN_PIN;

    SWITCH_GPIO->BSRR = 1 << SWITCH_PIN; // PIN high SWITCH on

#ifdef USE_BUTTON_EMERGENCY
    __disable_irq();
    AFIO->EXTICR[BUTTON_EXTICR_IDX] = BUTTON_EXTICR_VAL;
    EXTI->IMR  |= BUTTON_IMR;
    EXTI->FTSR |= BUTTON_FTSR;
    NVIC_EnableIRQ(BUTTON_IRQn);
    __enable_irq();
#endif

    state = INIT;
    systick_irqs = 0;
#ifdef USE_BUTTON_EMERGENCY
    systick_delay_irq(SystemCoreClock / EMERGENCY_SYSTEM_CORE_CLOCK_DIV); // 250 milli-seconds @8 MHz
#else
    systick_delay_irq(SystemCoreClock); // 1 second @8 MHz
#endif

    while (1) {
        __WFI();
        if (state == RED && systick_irqs == RED_IRQS) {
            systick_irqs    = 0;
            state           = GREEN;
            RED_GPIO->BSRR  = 1 << RED_PIN;   // PIN high RED off
            GREEN_GPIO->BRR = 1 << GREEN_PIN; // PIN low GREEN on
        }
        else if (state == GREEN && systick_irqs == GREEN_IRQS) {
            systick_irqs     = 0;
            state            = ORANGE;
            GREEN_GPIO->BSRR = 1 << GREEN_PIN;  // PIN high GREEN off
            ORANGE_GPIO->BRR = 1 << ORANGE_PIN; // PIN low ORANGE on
        }
        else if (state == ORANGE && systick_irqs == ORANGE_IRQS) {
            systick_irqs      = 0;
            state             = RED;
            ORANGE_GPIO->BSRR = 1 << ORANGE_PIN; // PIN high ORANGE off
            RED_GPIO->BRR     = 1 << RED_PIN;    // PIN low RED on
        }
        else if (state == INIT && systick_irqs == INIT_IRQS) {
            systick_irqs  = 0;
            state         = RED;
            RED_GPIO->BRR = 1 << RED_PIN; // PIN low RED on
        }
#ifdef USE_BUTTON_EMERGENCY
        else if (state == EMERGENCY) {
            SWITCH_GPIO->ODR ^= 1 << SWITCH_PIN; // toogle state
        }
#endif
    }

    return 0;
}

void
SysTick_Handler(void) {
    systick_irqs++;
}

#ifdef USE_BUTTON_EMERGENCY
void
BUTTON_HANDLER(void) {
    EXTI->PR |= BUTTON_EXTI_PR; // Reset irq

    if (state == EMERGENCY) {
        state             = RED;
        SWITCH_GPIO->BSRR = 1 << SWITCH_PIN; // PIN high SWITCH on
        ORANGE_GPIO->BSRR = 1 << ORANGE_PIN; // PIN high ORANGE off
        RED_GPIO->BRR     = 1 << RED_PIN;    // PIN low RED on
    }
    else {
        if (state == RED) {
            RED_GPIO->BSRR = 1 << RED_PIN; // PIN high RED off
        }
        else if (state == GREEN) {
            GREEN_GPIO->BSRR = 1 << GREEN_PIN; // PIN high GREEN off
        }
        state            = EMERGENCY;
        ORANGE_GPIO->BRR = 1 << ORANGE_PIN; // PIN low ORANGE on
    }
    systick_irqs = 0;
}
#endif
